
const txtFirstName = document.querySelector('#txt-first-name'); // target  and select 
console.log(txtFirstName);

const txtLastName = document.querySelector('#txt-last-name'); // target  and select 
console.log(txtLastName);

const spanFullName = document.querySelector('#span-full-name');
 

function fullName() {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);
